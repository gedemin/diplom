const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const image = require('gulp-image');
const del = require('del');
const BrowserSync = require('browser-sync');
gulp.task('html', function () {
    gulp.src('./src/**/*.html')
        .pipe(gulp.dest('./build/'));
});
gulp.task('sass', function () {
    return gulp.src('./src/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('style.css'))
        .pipe(autoprefixer({
            browsers: ['last 11 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./build/styles'));
});
gulp.task('image', function () {
    gulp.src('./src/**/*.svg')
        .pipe(image())
        .pipe(gulp.dest('./build/'));
});
gulp.task('reloader', function () {
    BrowserSync({
        server: {
            baseDir: './build/'
        }
    });
});
gulp.task('watch', ['image','sass', 'html', 'reloader'], function () {
    gulp.watch('./src/**/*.html', ['html']);
    gulp.watch('./build/**/*.html', BrowserSync.reload);
    gulp.watch('./src/**/*.scss', ['sass']);
    gulp.watch('./build/styles/**/*.css')
        .on('change', BrowserSync.reload);
    gulp.watch('./src/**/*.svg', [image]);
    gulp.watch('./build/**/*.svg', BrowserSync.reload);
});